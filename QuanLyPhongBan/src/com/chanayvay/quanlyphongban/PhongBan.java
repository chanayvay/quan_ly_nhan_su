package com.chanayvay.quanlyphongban;

public class PhongBan {
  private String sdt;
  private String diachi;
  private String tenPhongBan;
  private int maPhongBan;
  public PhongBan(String tenPhongBan,String diachi,String sdt,int maPhongBan){
    this.tenPhongBan = tenPhongBan;
    this.diachi = diachi;
    this.sdt = sdt;
    this.maPhongBan = maPhongBan;
  }

  public void setDiachi(String diachi) {
    this.diachi = diachi;
  }

  public void setMaPhongBan(int maPhongBan) {
    this.maPhongBan = maPhongBan;
  }

  public void setSdt(String sdt) {
    this.sdt = sdt;
  }

  public void setTenPhongBan(String tenPhongBan) {
    this.tenPhongBan = tenPhongBan;
  }

  public int getMaPhongBan() {
    return maPhongBan;
  }

  public String getSdt() {
    return sdt;
  }

  public String getDiachi() {
    return diachi;
  }

  public String getTenPhongBan() {
    return tenPhongBan;
  }
}
