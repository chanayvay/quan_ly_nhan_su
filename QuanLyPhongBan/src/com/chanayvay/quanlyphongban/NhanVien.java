package com.chanayvay.quanlyphongban;

public class NhanVien {
  int manv;
  String hoTen;
  String gioiTinh;
  String queQuan;
  String ngaySinh;
  String danToc;
  String sdt;
  String tenPB;
  String chucVu;
  int luong;
  String trinhDoHV;
  public NhanVien(int manv,String hoTen,String gioiTinh,String queQuan,String ngaySinh,
      String danToc,String sdt,String tenPB,String chucVu,int luong,String trinhDoHV){
    this.hoTen = hoTen;
    this.manv = manv;
    this.gioiTinh = gioiTinh;
    this.queQuan = queQuan;
    this.ngaySinh  = ngaySinh;
    this.danToc = danToc;
    this.sdt = sdt;
    this.tenPB = tenPB;
    this.chucVu = chucVu;
    this.luong = luong;
    this.trinhDoHV = trinhDoHV;
  }

}
