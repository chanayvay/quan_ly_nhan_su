package com.chanayvay.quanlyphongban;

import java.sql.SQLException;

public class Main {

  public static void main(String[] args) throws SQLException, ClassNotFoundException {
    QuanLyPhongBan quanLyPhongBan = new QuanLyPhongBan();
    QuanLyNhanVien quanLyNhanVien = new QuanLyNhanVien();
    boolean check = true;
    while (check) {
      System.out.println("1: Quản lý Phòng ban");
      System.out.println("2: Quản lý nhân viên");
      System.out.println("0: Thoát");
      System.out.println();
      System.out.print("Nhập vào lựa chọn: ");
      int k = Nhap.nhapSoNguyen();
      switch (k) {
        case 1:
          quanLyPhongBan.quanLy();
          break;
        case 2:
          quanLyNhanVien.quanLy();
          break;
        case 0:
          check = false;
          break;
          default:
            System.out.println("Chưa có chức năng ...");
            break;
      }
    }
  }
}
