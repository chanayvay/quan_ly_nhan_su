package com.chanayvay.quanlyphongban;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class QuanLyNhanVien {

  public void addNV(int manv, String hoTen, String gioiTinh, String queQuan, String ngaySinh,
      String danToc, String sdt, String tenPB, String chucVu, int luong, String trinhDoHV)
      throws SQLException, ClassNotFoundException {
    Connection connection = ConnectionUtils.getMyConnection();
    Statement statement = connection.createStatement();
    String sql = "insert into quanlynhanvien(manv, hoten,gioitinh,quequan,ngaysinh,"
        + "dantoc,sdt,tenpb,chucvu,luong,trinhdohocvan) "
        + " values(" + manv + ",'" + hoTen + "','" + gioiTinh + "','" + queQuan + "','" + ngaySinh
        + "','"
        + danToc + "','" + sdt + "','" + tenPB + "','" + chucVu + "'," + luong + ",'" + trinhDoHV
        + "')";

    ResultSet resultSet = statement.executeQuery("select tenpb from quanlyphongban" );
    boolean check = false;
    boolean ktraMaNV = true;
    while (resultSet.next()){
      if (resultSet.getString(1).equals(tenPB)){
        check = true;
        break;
      }
    }
    resultSet = statement.executeQuery("select manv from quanlynhanvien" );
    while (resultSet.next()){
      if (resultSet.getInt(1)==manv){
        ktraMaNV =false;
        break;
      }
    }
    if (check&&ktraMaNV) {
      statement.executeUpdate(sql);
    } else if (!check){
      System.out.println("Phòng ban không tồn tại ...");
    }
    else if (!ktraMaNV){
      System.out.println("Trùng mã nhân viên ....");
    }
  }

  public void fixNV(int manvfix, String hoTen, String gioiTinh,
      String queQuan, String ngaySinh, String danToc, String sdt, String
      tenPB, String chucVu, int luong, String trinhDoHV)
      throws SQLException, ClassNotFoundException {
    boolean check = true;
    Connection connection = ConnectionUtils.getMyConnection();
    Statement statement = connection.createStatement();
    String sql = "update quanlynhanvien"
        + " set hoten = '" + hoTen + "',"
        + "gioitinh = '" + gioiTinh + "',"
        + "quequan = '" + queQuan + "',"
        + "ngaysinh = '" + ngaySinh + "',"
        + "dantoc = '" + danToc + "',"
        + "sdt = '" + sdt + "',"
        + "tenpb = '" + tenPB + "',"
        + "chucvu = '" + chucVu + "',"
        + "luong = " + luong + ","
        + "trinhdohocvan = '" + trinhDoHV + "'"
        + " where manv = " + manvfix;
    String sqlrs = "select manv from quanlynhanvien";
    ResultSet rs = statement.executeQuery(sqlrs);

    while (rs.next()) {
      if (manvfix == rs.getInt(1)) {
        check = false;
        break;
      }
    }
    if (check) {
      System.out.println("Nhân viên không tồn tại ....");
    }
    else {
      statement.executeUpdate(sql);
    }
  }

  public void delNV(int manv) throws SQLException, ClassNotFoundException {
    boolean check = true;
    Connection connection = ConnectionUtils.getMyConnection();
    String sql = "delete from quanlynhanvien where manv = " + manv;
    Statement statement = connection.createStatement();
    String sqlrs = "select manv from quanlynhanvien";
    ResultSet rs = statement.executeQuery(sqlrs);
    while (rs.next()) {
      if (manv == rs.getInt(1)) {
        check = false;
        break;
      }
    }
    if (!check) {
      statement.executeUpdate(sql);
    } else {
      System.out.println("Nhân viên ko tồn tại ...");
    }
  }

  public void findNV(int manv) throws SQLException, ClassNotFoundException {
    Connection connection = ConnectionUtils.getMyConnection();
    Statement statement = connection.createStatement();
    String sql = "select manv, hoten,gioitinh,quequan,ngaysinh,"
        + "dantoc,sdt,tenpb,chucvu,luong,trinhdohocvan from quanlynhanvien";
    ResultSet rs = statement.executeQuery(sql);
    boolean check = true;
    while (rs.next()) {
      if (manv==rs.getInt(1)) {
        System.out.println("Mã nhân viên     : " + rs.getInt(1));
        System.out.println("Tên nhân viên    : " + rs.getString(2));
        System.out.println("Giới tính        : " + rs.getString(3));
        System.out.println("Quê quán         : " + rs.getString(4));
        System.out.println("Ngày sinh        : " + rs.getString(5));
        System.out.println("Dân tộc          : " + rs.getString(6));
        System.out.println("Số điện thoại    : " + rs.getString(7));
        System.out.println("Tên phòng ban    : " + rs.getString(8));
        System.out.println("Chức vụ          : " + rs.getString(9));
        System.out.println("Lương            : " + rs.getInt(10));
        System.out.println("Trình độ học vấn : " + rs.getString(4));
        check = false;
        break;
      }
    }
    if (check) {
      System.out.println("Nhân viên không tồn tại ......");
    }
  }

  public void danhSachNV() throws SQLException, ClassNotFoundException {
    System.out.println(
        "MÃ NV |HỌ TÊN            |GIỚI TÍNH |QUÊ QUÁN     |NGÀY SINH" 
            + "             |DÂN TỘC   |SỐ ĐIỆN THOẠI  |TÊN PHÒNG BAN" 
            + "       |CHỨC VỤ        |LƯƠNG     |TRÌNH ĐỘ HỌC VẤN ");
    Connection connection = ConnectionUtils.getMyConnection();
    Statement statement = connection.createStatement();
    String sql =
        "select manv, hoten,gioitinh,quequan,ngaysinh,dantoc,sdt,tenpb,chucvu,luong,trinhdohocvan from quanlynhanvien "
            + "order by manv asc";
    ResultSet rs = statement.executeQuery(sql);
    while (rs.next()) {
      String a = " ";
      String b = " ";
      String c = " ";
      String d = " ";
      String e = " ";
      String f = " ";
      String g = " ";
      String h = " ";
      String k = " ";
      String l = " ";

      for (int i = rs.getString(1).length(); i < 6; i++) {
        a += " ";
      }
      for (int i = rs.getString(2).length(); i < 18; i++) {
        b += " ";
      }
      for (int i = rs.getString(3).length(); i < 10; i++) {
        c += " ";
      }
      for (int i = rs.getString(4).length(); i < 13; i++) {
        d += " ";
      }
      for (int i = rs.getString(5).length(); i < 22; i++) {
        e += " ";
      }
      for (int i = rs.getString(6).length(); i < 10; i++) {
        f += " ";
      }
      for (int i = rs.getString(7).length(); i < 15; i++) {
        g += " ";
      }
      for (int i = rs.getString(8).length(); i < 20; i++) {
        h += " ";
      }
      for (int i = rs.getString(9).length(); i < 15; i++) {
        k += " ";
      }
      for (int i = rs.getString(10).length(); i < 10; i++) {
        l += " ";
      }

      System.out.printf(rs.getInt(1) + a + rs.getString(2) + b +
          rs.getString(3) + c + rs.getString(4)
          + d + rs.getString(5) + e + rs.getString(6) +
          f + rs.getString(7) + g + rs.getString(8)
          + h + rs.getString(9) + k + rs.getInt(10) + l + rs.getString(11));
      System.out.println();
    }
  }

  public void quanLy() throws SQLException, ClassNotFoundException {
    System.out.println("Các chức năng:");
    System.out.println("1: Thêm nhân viên");
    System.out.println("2: Xóa nhân viên");
    System.out.println("3: Sửa nhân viên");
    System.out.println("4: Tìm nhân viên");
    System.out.println("5: In ra danh sách nhân viên");
    System.out.println("0: Thoát");
    boolean check = true;
    while (check) {
      System.out.println();
      System.out.print("Nhập vào lựa chọn: ");
      int k = Nhap.nhapSoNguyen();
      switch (k) {
        case 1:
          System.out.print("Mã nhân viên: ");
          int manv = Nhap.nhapSoNguyen();
          System.out.print("Họ tên: ");
          String hoTen = Nhap.nhapString();
          System.out.print("Giới tính: ");
          String gioiTinh = Nhap.nhapString();
          System.out.print("Quê quán: ");
          String queQuan = Nhap.nhapString();
          System.out.print("Ngày sinh: ");
          String ngaySinh = Nhap.nhapNS();
          System.out.print("Dân tộc: ");
          String danToc = Nhap.nhapString();
          System.out.print("Số điện thoại: ");
          String sdt = Nhap.nhapString();
          System.out.print("Tên Phòng ban: ");
          String tenPB = Nhap.nhapString();
          System.out.print("Chức vụ: ");
          String chucVu = Nhap.nhapString();
          System.out.print("Lương: ");
          int luong = Nhap.nhapSoNguyen();
          System.out.print("Trình độ học vấn: ");
          String trinhDoHV = Nhap.nhapString();

          addNV(manv, hoTen, gioiTinh, queQuan, ngaySinh
              , danToc, sdt, tenPB, chucVu, luong, trinhDoHV);
          System.out.print("--------------------------------");
          break;
        case 2:
          System.out.print("Nhập mã nhân viên cần xóa: ");
          manv = Nhap.nhapSoNguyen();
          delNV(manv);
          System.out.println("-------------------");
          break;
        case 3:
          System.out.print("Nhập mã nv cần thay thế:");
          int manvfix = Nhap.nhapSoNguyen();
          System.out.print("Họ tên: ");
          hoTen = Nhap.nhapString();
          System.out.print("Giới tính: ");
          gioiTinh = Nhap.nhapString();
          System.out.print("Quê quán: ");
          queQuan = Nhap.nhapString();
          System.out.print("Ngày sinh: ");
          ngaySinh = Nhap.nhapNS();
          System.out.print("Dân tộc: ");
          danToc = Nhap.nhapString();
          System.out.print("Số điện thoại: ");
          sdt = Nhap.nhapString();
          System.out.print("Tên Phòng ban: ");
          tenPB = Nhap.nhapString();
          System.out.print("Chức vụ: ");
          chucVu = Nhap.nhapString();
          System.out.print("Lương: ");
          luong = Nhap.nhapSoNguyen();
          System.out.print("Trình độ học vấn: ");
          trinhDoHV = Nhap.nhapString();
          fixNV(manvfix, hoTen, gioiTinh, queQuan, ngaySinh
              , danToc, sdt, tenPB, chucVu, luong, trinhDoHV);
          System.out.print("-------------------");
          break;
        case 4:
          System.out.println("Nhập mã nhân viên cần tìm: ");
          manv = Nhap.nhapSoNguyen();
          findNV(manv);
          System.out.print("-------------------");
          break;
        case 5:
          danhSachNV();
          System.out.print("-----------------------------------------------------");
          break;
        case 0:
          check = false;
          break;
        default:
          System.out.println("Chưa có chức năng ...");
          break;
      }
    }
  }
}
