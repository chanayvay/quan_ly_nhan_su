package com.chanayvay.quanlyphongban;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class QuanLyPhongBan {

  public void addPB(String tenPB, String diaChi, String sdt, int maPB)
      throws SQLException, ClassNotFoundException {
    tenPB = tenPB.trim();
    diaChi = diaChi.trim();
    sdt =sdt.trim();
    Connection connection = ConnectionUtils.getMyConnection();
    Statement statement = connection.createStatement();
    String sql = "insert into quanlyphongban( tenpb,diachipb,sdt,mapb)"
        + "values('" + tenPB + "','" + diaChi + "','" + sdt + "'," + maPB + ")";
    boolean check = true;
    String sqlrs = "select tenpb,sdt from quanlyphongban";
    ResultSet rs = statement.executeQuery(sqlrs);
    while (rs.next()) {
      if (tenPB.equals(rs.getString(1))||sdt.equals(rs.getString(2))){
        System.out.println("Phòng ban đã tồn tại ....");
        check = false;
        break;
      }
    }
      if (check) {
        statement.executeUpdate(sql);
      }
    }


  public void fixPB(String tenPB, String tenPBMoi, String diaChiMoi, String sdtmoi, int maPBmoi)
  throws SQLException,ClassNotFoundException{
    boolean check = true;
    Connection connection = ConnectionUtils.getMyConnection();
    Statement statement = connection.createStatement();
    String sql = "update quanlyphongban"
        + " set tenpb ='"+tenPBMoi+"',"
        + "diachipb='"+diaChiMoi+"',"
        + "sdt ='"+sdtmoi+"',"
        + "mapb = '"+maPBmoi+"'"
        + " where tenpb = '"+tenPB+"'";
    String sqlrs = "select tenpb from quanlyphongban";
    ResultSet rs = statement.executeQuery(sqlrs);
    while (rs.next()){
      if (tenPB.equals(rs.getString(1))){
        check = false;
        break;
      }
    }
    if (check) {
      System.out.println("Phòng ban không tồn tại ....");
    } else
    {statement.executeUpdate(sql);
    sql = "update quanlynhanvien "
        + " set tenpb ='"+tenPBMoi+"' "
        + "where tenpb = '"+tenPB+"'";
    statement.executeUpdate(sql);
    }
  }

  public void delPB(String tenPB) throws SQLException, ClassNotFoundException {
    boolean check = true;
    Connection connection = ConnectionUtils.getMyConnection();
    String sql = "delete from quanlyphongban where tenpb = '"+tenPB+"'";
    Statement statement = connection.createStatement();
    String sqlrs = "select tenpb from quanlyphongban";
    ResultSet rs =statement.executeQuery(sqlrs);
    while (rs.next()){
      if (tenPB.equals(rs.getString(1))){
        check =false;
        break;
      }
    }
    if (!check) {
      statement.executeUpdate(sql);
    } else {
      System.out.println("Phòng ban ko tồn tại ...");
    }
  }
  public void findPB(String tenPB) throws SQLException,ClassNotFoundException{
    Connection connection = ConnectionUtils.getMyConnection();
    Statement statement = connection.createStatement();
    String sql = "select tenpb,diachipb,sdt,mapb from quanlyphongban";
    ResultSet rs = statement.executeQuery(sql);
    boolean check = true;
    while (rs.next()){
      if (tenPB.equals(rs.getString(1))){
        System.out.println("Tên phòng ban : "+rs.getString(1));
        System.out.println("Địa chỉ       : "+rs.getString(2));
        System.out.println("Số điện thoại : "+rs.getString(3));
        System.out.println("Mã phòng ban  : "+rs.getInt(4));
        System.out.println("Danh sách nhân viên trong phòng ban: ");
        sql = "select manv,hoten from quanlynhanvien qlnv "
            + "inner join quanlyphongban qlpb on qlnv.tenpb = qlpb.tenpb "
            + " where qlnv.tenpb = '"+tenPB+"'";
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()){
          System.out.print("Mã nv:  "+resultSet.getInt(1)+
              "        Tên nv: "+resultSet.getString(2));
          System.out.println();
        }
        check =false;
        break;
      }
    }

    if (check){
      System.out.println("Phòng ban không tồn tại ......");
    }
  }

  public void inPB() throws SQLException,ClassNotFoundException{
    System.out.println("PHÒNG BAN            ĐỊA CHỈ             SỐ ĐIỆN THOẠI       MÃ PHÒNG BAN ");
    Connection connection = ConnectionUtils.getMyConnection();
    Statement statement = connection.createStatement();
    String sql = "select tenpb,diachipb,sdt,mapb from quanlyphongban "
        + "order by diachipb asc";
    ResultSet rs = statement.executeQuery(sql);
    while (rs.next()){
      String a = " ";
      String b = " ";
      String c = " ";
      for (int i= rs.getString(1).length();i<20;i++){
        a+= " ";
      }
      for (int i= rs.getString(2).length();i<20;i++){
        b+= " ";
      }
      for (int i= rs.getString(3).length();i<20;i++){
        c+= " ";
      }

      System.out.printf(rs.getString(1)+a+rs.getString(2)+b+
      rs.getString(3)+c+rs.getString(4));
      System.out.println();
    }
  }
  public void quanLy() throws SQLException, ClassNotFoundException {
    System.out.println("Các chức năng:");
    System.out.println("1: Thêm Phòng ban");
    System.out.println("2: Xóa Phòng ban");
    System.out.println("3: Sửa Phòng ban");
    System.out.println("4: Tìm Phòng ban");
    System.out.println("5: In ra danh sách Phòng ban");
    System.out.println("0: Thoát");
    boolean check = true;
    while (check){
      System.out.println();
      System.out.print("Nhập vào lựa chọn: ");
      int k = Nhap.nhapSoNguyen();
      switch (k){
        case 1:
          System.out.println("Nhập tên Phòng ban: ");
          String name = Nhap.nhapString();
          System.out.println("Nhập địa chỉ");
          String diachi = Nhap.nhapString();
          System.out.println("Nhập số điện thoại");
          String sdt = Nhap.nhapString();
          System.out.println("Nhập mã phòng ban");
          int maPB = Nhap.nhapSoNguyen();

          addPB(name,diachi,sdt,maPB);
          System.out.print("------------------");
          break;
        case 2:
          System.out.println("Nhập tên phòng ban cần xóa: ");
          String nameDel = Nhap.nhapString();
          delPB(nameDel);
          System.out.println("-------------------");
          break;
        case 3:
          System.out.println("Nhập tên phòng ban cần thay thế:");
          String tenSua = Nhap.nhapString();
          System.out.println("Nhập tên mới: ");
          name = Nhap.nhapString();
          System.out.println("Nhập địa chỉ mới: ");
          diachi = Nhap.nhapString();
          System.out.println("Nhập sdt mới: ");
          sdt = Nhap.nhapString();
          System.out.println("Nhập mã mới: ");
          maPB = Nhap.nhapSoNguyen();
          fixPB(tenSua,name,diachi,sdt,maPB);
          System.out.print("-------------------");
          break;
        case 4:
          System.out.println("Nhập tên phòng ban cần tìm: ");
          name = Nhap.nhapString();
          findPB(name);
          System.out.print("-------------------");
          break;
        case 5:
          inPB();
          System.out.print("-------------------");
          break;
        case 0:
          check = false;
          break;
        default:
          System.out.println("Chưa có chức năng ...");
          break;
      }
    }
  }
}
